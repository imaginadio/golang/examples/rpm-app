package main

import (
	"container/list"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Rps struct {
	rpsQueue *list.List
	rpsLimit int
}

// не считаем запросы старше 1 секунды
func (rps *Rps) cutOldRequests() {
	now := time.Now()
	for rps.rpsQueue.Len() > 0 {
		e := rps.rpsQueue.Front()
		t := e.Value.(time.Time)
		if now.Sub(t).Milliseconds() > 1000 {
			rps.rpsQueue.Remove(e)
		} else {
			return
		}
	}
}

// получение RPS
func (rps *Rps) getRps() int {
	rps.cutOldRequests()
	return rps.rpsQueue.Len()
}

// проверка на достижение лимита RPS
func (rps *Rps) checkLimit() bool {
	return (rps.getRps() < rps.rpsLimit)
}

// добавление нового запроса в учет RPS
func (rps *Rps) addRequest() {
	rps.rpsQueue.PushBack(time.Now())
	rps.cutOldRequests()
}

func main() {
	router := gin.New()
	rps := Rps{
		rpsQueue: list.New(),
		rpsLimit: 100,
	}

	// метод получения текущего RPS
	router.GET("/rps", func(c *gin.Context) {
		c.String(http.StatusOK, "%v", rps.getRps())
	})

	// метод, проверяющий лимит запросов
	router.GET("/limit", func(c *gin.Context) {
		c.String(http.StatusOK, "%t", rps.checkLimit())
	})

	// метод, занимающий место в окне
	router.GET("/load", func(c *gin.Context) {
		rps.addRequest()
		c.String(http.StatusOK, "job done")
	})

	router.Run(":8080")
}
